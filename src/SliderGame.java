import java.util.Scanner;

public class SliderGame {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int exit = 1;
		do {
			int choice = 0;
			System.out.println("Hello to the board solver.");
			Board a = new Board();

			int[] values = new int[9];
			System.out.println("Please enter 9 values for your board");
			for (int i = 0; i < 9; i++)
				values[i] = scan.nextInt();
			a.makeBoard(values);
			System.out.println("Would you prefer to \n 1. Solve the board using the A* Method \t or \t 2. Solve the board using the Brute Force Method");
			choice = scan.nextInt();
			while (choice != 1 && choice != 2) {
				System.out.println("My apologies, but your input isn't recognized");
				System.out.println("Would you prefer to \n 1. Solve the board using the A* Method \t or \t 2. Solve the board using the Brute Force Method");
				choice = scan.nextInt();
			}
			switch (choice) {
			case 1:
				System.out.println("Now the program will solve your board, hopefully");
				scan.nextLine();
				aStarSolver.puzzleSolver(a);
				break;

			case 2:
				
				System.out.println("Now the program will solve your board, hopefully");
				scan.nextLine();
				bruteForceSolve.puzzleSolver(a);
				break;
			}

			System.out.println(
					"Thank you for playing, would you like to play again? \nType 0 to Exit, anything else to continue");
			exit = scan.nextInt();
		} while (exit != 0);
		scan.close();
	}
}


/* In Part 2, the board that is the closest to being solved is board 4
 * the comment below is each of the four being done
 * 
 */

//Board 1
/*Hello to the board solver.
	Please enter 9 values for your board
	4 0 1 3 5 2 6 8 7
	Would you prefer to 
	1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
	1
	Now the program will solve your board, hopefully
	Show Me
	4 0 1 
	3 5 2 
	6 8 7 
	L==>
	4 1 0 
	3 5 2 
	6 8 7 
	U==>
	4 1 2 
	3 5 0 
	6 8 7 
	R==>
	4 1 2 
	3 0 5 
	6 8 7 
	U==>
	4 1 2 
	3 8 5 
	6 0 7 
	L==>
	4 1 2 
	3 8	5 
	6 7	0 
	D==>
	4 1 2 
	3 8 0 
	6 7 5 
	R==>
	4 1 2 
	3 0 8 
	6 7 5 
	R==>
	4 1 2 
	0 3 8 
	6 7 5 
	U==>
	4 1 2 
	6 3 8 
	0 7 5 
	L==>
	4 1 2 
	6 3 8 
	7 0 5 
	L==>
	4 1 2 
	6 3 8 
	7 5 0 
	D==>	
	4 1 2 
	6 3 0 
	7 5 8 
	R==>
	4 1 2 
	6 0 3 
	7 5 8 
	R==>
	4 1 2 
	0 6 3 
	7 5 8 
	D==>
	0 1 2 
	4 6 3 
	7 5 8 
	L==>
	1 0 2 
	4 6 3 
	7 5 8 
	L==>
	1 2 0 
	4 6 3 
	7 5 8 
	U==>
	1 2 3 
	4 6 0 
	7 5 8 
	R==>
	1 2 3 
	4 0 6 
	7 5 8 
	U==>
	1 2 3 
	4 5 6 
	7 0 8 
	L==>
	1 2 3 
	4 5 6 
	7 8 0 
	=>Show Me moves Required: 21
	Queue Added= 27567 Remove= 15784 CURR SIZE= 11783
	Thank you for playing, would you like to play again? 
	Type 0 to Exit, anything else to continue*/

/*
Hello to the board solver.
Please enter 9 values for your board
1 3 8 6 2 0 5 4 7
Would you prefer to 
 1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
1
Now the program will solve your board, hopefully
Show Me
1 3 8 
6 2 0 
5 4 7 
D==>
1 3 0 
6 2 8 
5 4 7 
R==>
1 0 3 
6 2 8 
5 4 7 
U==>
1 2 3 
6 0 8 
5 4 7 
R==>
1 2 3 
0 6 8 
5 4 7 
U==>
1 2 3 
5 6 8 
0 4 7 
L==>
1 2 3 
5 6 8 
4 0 7 
L==>
1 2 3 
5 6 8 
4 7 0 
D==>
1 2 3 
5 6 0 
4 7 8 
R==>
1 2 3 
5 0 6 
4 7 8 
R==>
1 2 3 
0 5 6 
4 7 8 
U==>
1 2 3 
4 5 6 
0 7 8 
L==>
1 2 3 
4 5 6 
7 0 8 
L==>
1 2 3 
4 5 6 
7 8 0 
=>Show Me moves Required: 13
Queue Added= 445 Remove= 263 CURR SIZE= 182
Thank you for playing, would you like to play again? 
Type 0 to Exit, anything else to continue
*/

/*
* Hello to the board solver.
* Please enter 9 values for your board
* 1 3 2 4 5 6 8 7 0
* Would you prefer to 
* 1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
* 1
* Now the program will solve your board, hopefully
Show Me
1 3 2 
4 5 6 
8 7 0 
D==>
1 3 2 
4 5 0 
8 7 6 
R==>
1 3 2 
4 0 5 
8 7 6 
D==>
1 0 2 
4 3 5 
8 7 6 
R==>
0 1 2 
4 3 5 
8 7 6 
U==>
4 1 2 
0 3 5 
8 7 6 
U==>
4 1 2 
8 3 5 
0 7 6 
L==>
4 1 2 
8 3 5 
7 0 6 
L==>
4 1 2 
8 3 5 
7 6 0 
D==>
4 1 2 
8 3 0 
7 6 5 
R==>
4 1 2 
8 0 3 
7 6 5 
R==>
4 1 2 
0 8 3 
7 6 5 
D==>
0 1 2 
4 8 3 
7 6 5 
L==>
1 0 2 
4 8 3 
7 6 5 
L==>
1 2 0 
4 8 3 
7 6 5 
U==>
1 2 3 
4 8 0 
7 6 5 
U==>
1 2 3 
4 8 5 
7 6 0 
R==>
1 2 3 
4 8 5 
7 0 6 
D==>
1 2 3 
4 0 5 
7 8 6 
L==>
1 2 3 
4 5 0 
7 8 6 
U==>
1 2 3 
4 5 6 
7 8 0 
=>Show Me moves Required: 20
Queue Added= 20077 Remove= 11566 CURR SIZE= 8511
Thank you for playing, would you like to play again? 
Type 0 to Exit, anything else to continue
*/

/*
Hello to the board solver.
Please enter 9 values for your board
1 2 3 7 4 0 6 5 8
Would you prefer to 
 1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
1
Now the program will solve your board, hopefully
Show Me
1 2 3 
7 4 0 
6 5 8 
U==>
1 2 3 
7 4 8 
6 5 0 
R==>
1 2 3 
7 4 8 
6 0 5 
R==>
1 2 3 
7 4 8 
0 6 5 
D==>
1 2 3 
0 4 8 
7 6 5 
L==>
1 2 3 
4 0 8 
7 6 5 
L==>
1 2 3 
4 8 0 
7 6 5 
U==>
1 2 3 
4 8 5 
7 6 0 
R==>
1 2 3 
4 8 5 
7 0 6 
D==>
1 2 3 
4 0 5 
7 8 6 
L==>
1 2 3 
4 5 0 
7 8 6 
U==>
1 2 3 
4 5 6 
7 8 0 
=>Show Me moves Required: 11
Queue Added= 253 Remove= 148 CURR SIZE= 105*/


//Below is the output for the brute force method
//This is the brute force method for board 1
//Hello to the board solver.
//Please enter 9 values for your board
//4 0 1 3 5 2 6 8 7
//Would you prefer to 
//1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//2
//Now the program will solve your board, hopefully
//Show Me
//4 0 1 
//3 5 2 
//6 8 7 
//L==>
//4 1 0 
//3 5 2 
//6 8 7 
//U==>
//4 1 2 
//3 5 0 
//6 8 7 
//R==>
//4 1 2 
//3 0 5 
//6 8 7 
//U==>
//4 1 2 
//3 8 5 
//6 0 7 
//L==>
//4 1 2 
//3 8 5 
//6 7 0 
//D==>
//4 1 2 
//3 8 0 
//6 7 5 
//R==>
//4 1 2 
//3 0 8 
//6 7 5 
//R==>
//4 1 2 
//0 3 8 
//6 7 5 
//U==>
//4 1 2 
//6 3 8 
//0 7 5 
//L==>
//4 1 2 
//6 3 8 
//7 0 5 
//L==>
//4 1 2 
//6 3 8 
//7 5 0 
//D==>
//4 1 2 
//6 3 0 
//7 5 8 
//R==>
//4 1 2 
//6 0 3 
//7 5 8 
//R==>
//4 1 2 
//0 6 3 
//7 5 8 
//D==>
//0 1 2 
//4 6 3 
//7 5 8 
//L==>
//1 0 2 
//4 6 3 
//7 5 8 
//L==>
//1 2 0 
//4 6 3 
//7 5 8 
//U==>
//1 2 3 
//4 6 0 
//7 5 8 
//R==>
//1 2 3 
//4 0 6 
//7 5 8 
//U==>
//1 2 3 
//4 5 6 
//7 0 8 
//L==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 21
//Queue Added= 440625 Remove= 246816 CURR SIZE= 193809

//This is the brute force method for board 2
//Hello to the board solver.
//Please enter 9 values for your board
//1 3 8 6 2 0 5 4 7
//Would you prefer to 
//1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//2
//Now the program will solve your board, hopefully
//Show Me
//1 3 8 
//6 2 0 
//5 4 7 
//D==>
//1 3 0 
//6 2 8 
//5 4 7 
//R==>
//1 0 3 
//6 2 8 
//5 4 7 
//U==>
//1 2 3 
//6 0 8 
//5 4 7 
//R==>
//1 2 3 
//0 6 8 
//5 4 7 
//U==>
//1 2 3 
//5 6 8 
//0 4 7 
//L==>
//1 2 3 
//5 6 8 
//4 0 7 
//L==>
//1 2 3 
//5 6 8 
//4 7 0 
//D==>
//1 2 3 
//5 6 0 
//4 7 8 
//R==>
//1 2 3 
//5 0 6 
//4 7 8 
//R==>
//1 2 3 
//0 5 6 
//4 7 8 
//U==>
//1 2 3 
//4 5 6 
//0 7 8 
//L==>
//1 2 3 
//4 5 6 
//7 0 8 
//L==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 13
//Queue Added= 8369 Remove= 4962 CURR SIZE= 3407

//This is the brute force method for board 3
//Hello to the board solver.
//Please enter 9 values for your board
//1 3 2 4 5 6 8 7 0
//Would you prefer to 
//1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//2
//Now the program will solve your board, hopefully
//Show Me
//1 3 2 
//4 5 6 
//8 7 0 
//R==>
//1 3 2 
//4 5 6 
//8 0 7 
//D==>
//1 3 2 
//4 0 6 
//8 5 7 
//D==>
//1 0 2 
//4 3 6 
//8 5 7 
//R==>
//0 1 2 
//4 3 6 
//8 5 7 
//U==>
//4 1 2 
//0 3 6 
//8 5 7 
//U==>
//4 1 2 
//8 3 6 
//0 5 7 
//L==>
//4 1 2 
//8 3 6 
//5 0 7 
//L==>
//4 1 2 
//8 3 6 
//5 7 0 
//D==>
//4 1 2 
//8 3 0 
//5 7 6 
//R==>
//4 1 2 
//8 0 3 
//5 7 6 
//R==>
//4 1 2 
//0 8 3 
//5 7 6 
//U==>
//4 1 2 
//5 8 3 
//0 7 6 
//L==>
//4 1 2 
//5 8 3 
//7 0 6 
//D==>
//4 1 2 
//5 0 3 
//7 8 6 
//R==>
//4 1 2 
//0 5 3 
//7 8 6 
//D==>
//0 1 2 
//4 5 3 
//7 8 6 
//L==>
//1 0 2 
//4 5 3 
//7 8 6 
//L==>
//1 2 0 
//4 5 3 
//7 8 6 
//U==>
//1 2 3 
//4 5 0 
//7 8 6 
//U==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 20
//Queue Added= 281054 Remove= 163861 CURR SIZE= 117193

//This is the brute force method for board 4
//Hello to the board solver.
//Please enter 9 values for your board
//1 2 3 7 4 0 6 5 8
//Would you prefer to 
//1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//2
//Now the program will solve your board, hopefully
//Show Me
//1 2 3 
//7 4 0 
//6 5 8 
//U==>
//1 2 3 
//7 4 8 
//6 5 0 
//R==>
//1 2 3 
//7 4 8 
//6 0 5 
//R==>
//1 2 3 
//7 4 8 
//0 6 5 
//D==>
//1 2 3 
//0 4 8 
//7 6 5 
//L==>
//1 2 3 
//4 0 8 
//7 6 5 
//L==>
//1 2 3 
//4 8 0 
//7 6 5 
//U==>
//1 2 3 
//4 8 5 
//7 6 0 
//R==>
//1 2 3 
//4 8 5 
//7 0 6 
//D==>
//1 2 3 
//4 0 5 
//7 8 6 
//L==>
//1 2 3 
//4 5 0 
//7 8 6 
//U==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 11
//Queue Added= 2321 Remove= 1380 CURR SIZE= 941



//					Part 2 of Part 2	
//			The output for the board  7 6 4 
//									  0 8 1 
//									  2 3 5
//			The A* method is almost instantaneous, whereas the bruteforce method 
//			takes almost 30 seconds to run on my computer
/*****************************************************************************************************
 **							**					**	*		*	**	*	*	*	*	*	*	*	*	**
 **																									**
 ** 																								**
 ** 																								**	
 ** 																								**
 ** 																								**
 **																									**
 * *																								**
 * *																								**
 * *																								**
 *****************************************************************************************************/

//This is the new version
//Hello to the board solver.
//Please enter 9 values for your board
//7 6 4 0 8 1 2 3 5
//Would you prefer to 
// 1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//1
//Now the program will solve your board, hopefully
//Show Me
//7 6 4 
//0 8 1 
//2 3 5 
//L==>
//7 6 4 
//8 0 1 
//2 3 5 
//D==>
//7 0 4 
//8 6 1 
//2 3 5 
//L==>
//7 4 0 
//8 6 1 
//2 3 5 
//U==>
//7 4 1 
//8 6 0 
//2 3 5 
//R==>
//7 4 1 
//8 0 6 
//2 3 5 
//R==>
//7 4 1 
//0 8 6 
//2 3 5 
//D==>
//0 4 1 
//7 8 6 
//2 3 5 
//L==>
//4 0 1 
//7 8 6 
//2 3 5 
//L==>
//4 1 0 
//7 8 6 
//2 3 5 
//U==>
//4 1 6 
//7 8 0 
//2 3 5 
//R==>
//4 1 6 
//7 0 8 
//2 3 5 
//U==>
//4 1 6 
//7 3 8 
//2 0 5 
//R==>
//4 1 6 
//7 3 8 
//0 2 5 
//D==>
//4 1 6 
//0 3 8 
//7 2 5 
//D==>
//0 1 6 
//4 3 8 
//7 2 5 
//L==>
//1 0 6 
//4 3 8 
//7 2 5 
//U==>
//1 3 6 
//4 0 8 
//7 2 5 
//U==>
//1 3 6 
//4 2 8 
//7 0 5 
//L==>
//1 3 6 
//4 2 8 
//7 5 0 
//D==>
//1 3 6 
//4 2 0 
//7 5 8 
//D==>
//1 3 0 
//4 2 6 
//7 5 8 
//R==>
//1 0 3 
//4 2 6 
//7 5 8 
//U==>
//1 2 3 
//4 0 6 
//7 5 8 
//U==>
//1 2 3 
//4 5 6 
//7 0 8 
//L==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 25
//Queue Added= 230715 Remove= 132918 CURR SIZE= 97797



//This is the bruteforce version
//Hello to the board solver.
//Please enter 9 values for your board
//7 6 4 0 8 1 2 3 5
//Would you prefer to 
// 1. Solve the board using the A* Method 	 or 	 2. Solve the board using the Brute Force Method
//2
//Now the program will solve your board, hopefully
//Show Me
//7 6 4 
//0 8 1 
//2 3 5 
//L==>
//7 6 4 
//8 0 1 
//2 3 5 
//D==>
//7 0 4 
//8 6 1 
//2 3 5 
//L==>
//7 4 0 
//8 6 1 
//2 3 5 
//U==>
//7 4 1 
//8 6 0 
//2 3 5 
//R==>
//7 4 1 
//8 0 6 
//2 3 5 
//R==>
//7 4 1 
//0 8 6 
//2 3 5 
//D==>
//0 4 1 
//7 8 6 
//2 3 5 
//L==>
//4 0 1 
//7 8 6 
//2 3 5 
//L==>
//4 1 0 
//7 8 6 
//2 3 5 
//U==>
//4 1 6 
//7 8 0 
//2 3 5 
//R==>
//4 1 6 
//7 0 8 
//2 3 5 
//U==>
//4 1 6 
//7 3 8 
//2 0 5 
//R==>
//4 1 6 
//7 3 8 
//0 2 5 
//D==>
//4 1 6 
//0 3 8 
//7 2 5 
//D==>
//0 1 6 
//4 3 8 
//7 2 5 
//L==>
//1 0 6 
//4 3 8 
//7 2 5 
//U==>
//1 3 6 
//4 0 8 
//7 2 5 
//U==>
//1 3 6 
//4 2 8 
//7 0 5 
//L==>
//1 3 6 
//4 2 8 
//7 5 0 
//D==>
//1 3 6 
//4 2 0 
//7 5 8 
//D==>
//1 3 0 
//4 2 6 
//7 5 8 
//R==>
//1 0 3 
//4 2 6 
//7 5 8 
//U==>
//1 2 3 
//4 0 6 
//7 5 8 
//U==>
//1 2 3 
//4 5 6 
//7 0 8 
//L==>
//1 2 3 
//4 5 6 
//7 8 0 
//=>Show Me moves Required: 25
//Queue Added= 4704631 Remove= 2709000 CURR SIZE= 1995631


