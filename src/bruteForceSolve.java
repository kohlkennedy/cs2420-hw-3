public class bruteForceSolve {
	//Trevor Harshman provided me his code from assignment 1
	//This method is based off of his linked list solver
    //Method that is called to solve the given board
    public static void puzzleSolver(Board b) {

        //Creates queue for storing the different board states
        LinkedQueue<BoardTracker> q = new LinkedQueue();

        //Variable for storing the data of how to board was solved
        //Initialized as null so that it is known to be empty
        BoardTracker solvedBoardTracker = null;

        //creates BoardTracker for the board passed through, then adds it to the queue
        BoardTracker moveInfo = new BoardTracker(b);
        q.enqueue(moveInfo);

        //Creates a board of all zero values. This board is to facilitate a stopping condition for when the board
        //is unsolvable.
        Board nullBoard = new Board();
        int[] nullValues = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        nullBoard.makeBoard(nullValues);

        while(solvedBoardTracker == null) {

            //As long as solvedBoardTracker is null (not solved), the method puzzleQueue will continue to be run
            solvedBoardTracker = puzzleQueue(q);
        }

        //If the board is unsolvable a BoardTracker that's got the nullBoard(Board of all zeroes)
        //will be returned, then trigger puzzleSolver to end
        if(solvedBoardTracker.getBoard() == nullBoard){
            return;
        }

        //If solvedBoardTracker is given a value other than null and nullBoard, then the showMe method is called
        //and the solution and statistics are shown
        else{
            showMe(b, solvedBoardTracker.getMovesMade(), solvedBoardTracker.getMoveCount());
            int currentSize = q.queueAdded - q.queueRemoved;
            System.out.printf("Queue Added= %d Remove= %d CURR SIZE= %d%n", q.queueAdded, q.queueRemoved, currentSize);
        }
    }

    //method that dequeues a BoardTracker from the queue(which is passed through), makes any possible moves on it
    //then adds those moves back into the queue if they are not solutions
    private static BoardTracker puzzleQueue(LinkedQueue<BoardTracker> q) {

        //dequeues a BoardTracker
        BoardTracker boardInfo = q.dequeue();

        //If the dequeue returns null, then the queue is empty. Should not occur, but good to have in case it does
        if (boardInfo == null) {
            System.out.println("LinkedQueue is empty");
            return null;
        }

        if (!boardCheck(boardInfo)) {

            //Array for holding the 4 move options, that way a for loop can be used to go through each move
            char[] moveList = {'L', 'R', 'U', 'D'};

            //Loops through the 4 possible moves and affects individual deep copies of the given BoardTracker
            for (int i = 0; i < 4; i++) {

                //Creation of deep copy of boardInfo
                BoardTracker boardInfoCopy = new BoardTracker(new Board(boardInfo.getBoard()),
                        boardInfo.getMoveCount(), boardInfo.getMovesMade(), boardInfo.getLastMove() );

                //Makes move on the deep copy, boardInfoCopy, and if the move is valid and not solved, it is enqueued
                if (boardInfoCopy.moveBoard(moveList[i])) {

                    q.enqueue(boardInfoCopy);

                    //The move count is incremented and the last move is updated
                    boardInfoCopy.incrementMoveCount();
                    boardInfoCopy.changeLastMove(moveList[i]);

                    //If over 31 moves are made, which is the most moves possible for a solution,
                    // then the board is not solvable and nullBoard is returned, signalling to method puzzleSolver
                    //that there is no solution
                    if(boardInfoCopy.getMoveCount() > 32){
                        System.out.println("The puzzle is not solvable.");

                        int[] nullValues = {0, 0, 0, 0, 0, 0, 0, 0, 0};
                        Board nullBoard = new Board();
                        nullBoard.makeBoard(nullValues);

                        BoardTracker nullTracker = new BoardTracker(nullBoard);

                        return nullTracker;
                    }
                }
            }
        }

        //If the board is solved, it is returned to method puzzleSolver
        if(boardCheck(boardInfo)) {
            return boardInfo;
        }

        //If there is no solution to return, null is returned
        return null;
    }

    //Checks to see if a board is a solved board
    //Takes in BoardTracker and pulls the board from that.
    private static boolean boardCheck(BoardTracker boardInfo) {

        //Initializes the solved board so it can be used for comparison.
        Board solvedBoard = new Board();
        int[] solvedValues = {1, 2, 3, 4, 5, 6, 7, 8, 0};
        solvedBoard.makeBoard(solvedValues);

        Board board = boardInfo.getBoard();

        //If the board being tested equals solvedBoard then true is returned, otherwise false.
        if (board.equals(solvedBoard)) {
            return true;
        } else {
            return false;
        }
    }

    //Method for showing a set of moves to reach a solution
    public static void showMe(Board b, String moves, int moveCount) {
        //Converts string of moves into an array
        char[] moveList = moves.toCharArray();

        //Print original board state
        System.out.println("Show Me");
        System.out.print(b);

        for (int i = 0; i < moveList.length; i++) {

            //box the current move from a char to Character so it can be compared
            Character move = moveList[i];

            //If move is L
            if (move.compareTo('L') == 0) {
                if (b.slideLeft()) {
                    System.out.printf("L==>%n%S", b);
                }
            }

            //If move is R
            else if (move.compareTo('R') == 0) {
                if(b.slideRight()){
                    System.out.printf("R==>%n%S", b);
                }
            }

            //If move is U
            else if (move.compareTo('U') == 0) {
                if(b.slideUp()){
                    System.out.printf("U==>%n%S", b);
                }
            }

            //If move is D
            else if (move.compareTo('D') == 0) {
                if(b.slideDown()){
                    System.out.printf("D==>%n%S", b);
                }

            } else {
                System.out.println("Invalid move occurred.");
            }
        }
        System.out.printf("=>Show Me moves Required: %d%n", moveCount);
    }
}