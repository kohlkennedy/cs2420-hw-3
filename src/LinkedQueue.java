public class LinkedQueue<T> {
    //Variable that keeps track of first node
    private ListElement<T> head;

    //Variable that keeps track of last node
    private ListElement<T> tail;

    //Variables for keeping track of how many queues are added and hour many are removed
    protected int queueAdded ;
    protected int queueRemoved;

    //Public default constructor for the Linked List Queue
    public LinkedQueue(){
        head = null;
        tail = null;
    }

    //Adds a node onto the end of the queue, then increments the queueAdd variable
    public void enqueue(T element){
        if (empty()){
            head = new ListElement(element);
            tail = head;
        }
        else {
            tail.nextElement = new ListElement(element);
            tail = tail.nextElement;
        }
        queueAdded++;
    }

    //removes head of the queue and returns it, then sets the next node in the queue as the head
    //Increments the queueRemoved variable
    public T dequeue(){

        //Returns null if the queue is empty
        if (empty()) {
            System.out.println("LinkedQueue is empty");
            return null;
        }

        T firstElement = front();
        head = head.nextElement;
        if (head == null) {
            tail = null;
        }
        queueRemoved++;
        return firstElement;
    }

    //Returns the value of the front node
    public T front(){
        if (empty()){
            return null;
        }
        return head.element;
    }

    //Checks if the queue is empty by seeing if head is equal to null
    public boolean empty(){
        return (head == null);
    }

    //Removes all values from the queue
    public void clear(){
        while(head != null){
            dequeue();
        }
    }

    // Class that contains Linked List code
    public class ListElement<T>{

        //element is a "node" of the linked list
        private T element = null;
        private ListElement<T> nextElement = null;

        //sets variable private element equal to passed value
        public ListElement(T element){
            this.element = element;
        }

        //Used to get an element
        public T getElement(){
            return element;
        }

        //Gives the value of the element that is next from element (I.E element.getNextElement() returns the element
        //linked to be next)
        public ListElement<T> getNextElement(){
            return nextElement;
        }

        //Used to link the elements together
        public void setNextElement(ListElement<T> nextElement){
            this.nextElement = nextElement;
        }
    }
}