public class BoardTracker implements Comparable<BoardTracker> {
    // stores # of moves made
    private int moveCount;

    // String that holds the letters that correspond to the moves made
    private String movesMade;

    // stores the last move that was made
    private char lastMove;

    // stores the board that this data is about
    private Board board;

    private int boardPriority;
    //Calculates the priority of the Board
    public int getBoardPriority()
    {
    	//Sets the priority
        this.boardPriority = moveCount + board.boardHammingDistance();
        //Returns the prio
        return boardPriority;
    }

    // Default constructor, sets board equal to a solved board
    public BoardTracker() {
        int[] defaultInts = { 1, 2, 3, 4, 5, 6, 7, 8, 0 };
        Board b = new Board();
        b.makeBoard(defaultInts);
        this.moveCount = 0;
        this.movesMade = "";
        
    }

    // Overloaded constructor that take just a board value. Sets MoveCount to 0 and
    // MovesMade to ""
    public BoardTracker(Board b) {
        this.board = b;
        this.moveCount = 0;
        this.movesMade = "";
        this.boardPriority = 0;
    }

    // Overloaded constructor that allows for moveCount, movesMade, and lastMove to
    // be passed as well as board.
    public BoardTracker(Board board, int moveCount, String movesMade, char lastMove) {
        this.board = new Board(board);
        this.moveCount = moveCount;
        this.movesMade = movesMade;
        this.lastMove = lastMove;
        this.boardPriority = getBoardPriority();
    }

    // Increments the move count, imagine that
    public void incrementMoveCount() {
        moveCount += 1;
    }

    // Updates variable lastMove and appends it to the movesMade string
    public void changeLastMove(char c) {
        this.lastMove = c;
        movesMade = movesMade.concat(Character.toString(c));
    }

    // Allows for the private Board to be moved, then returns true if the movement
    // was successful, and false if not
    public boolean moveBoard(char move) {
        if (board.makeMove(move, lastMove) == 'm') {
            return true;
        } else
            return false;
    }

    // returns moveCount
    public int getMoveCount() {
        return moveCount;
    }

    // returns moveMade
    public String getMovesMade() {
        return movesMade;
    }

    // returns lastMove
    public char getLastMove() {
        return lastMove;
    }

    // returns board
    public Board getBoard() {
        return board;
    }

    // Overridden toString method that makes it easier to follow the data when
    // debugging
    @Override
    public String toString() {
        return String.format(board + "Total moves made: %d%nList of moves made: %s%n", moveCount, movesMade);

    }
    //This is the compare function, it compares the distance and moves left
    @Override
    public int compareTo(BoardTracker board) {
        //If move is better, gets added to the stack
        if (boardPriority > board.boardPriority)
            return 1;
    	//If the move is worse, it is lowered
        if (boardPriority < board.boardPriority)
            return -1;
        //If the priority is the same, leave as same
        else
            return 0;
    }
}
